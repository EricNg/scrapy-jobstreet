from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from tutorial.items import JobItem

class JobSpider(CrawlSpider):
    name = "jobcentral"
    allowed_domains = ["jobscentral.com.sg"]
    start_urls = [
        "http://jobscentral.com.sg/jc/jobseeker/jobs/JobResults.aspx"
    ]

    rules = (

        # Extract links matching 'item.php' and parse them with the spider's method parse_item
        Rule(SgmlLinkExtractor(allow=('http://jobscentral.com.sg/JC/JobSeeker/Jobs/JobResults.aspx', )), callback='parse_item',  follow= True),
    )
	
    def parse_item(self, response):
        sel = Selector(response)
        sites = sel.xpath('//*[@class="jobWrapper"]')
        items = []
        
	for site in sites:
            item = JobItem()
            item['jobtitle'] = site.xpath('div[@class="jobdetailWrapper"]/div[contains(@class, "jobListJInfo") and contains(@class, "div1")]/h2/a/text()').extract()
            item['company'] = site.xpath('div[@class="jobdetailWrapper"]/div[contains(@class, "jobListJInfo") and contains(@class, "div2")]/a/text()').extract()
            item['industry'] = site.xpath('div[@class="jobdetailWrapper"]/div[contains(@class, "jobListJInfo") and contains(@class, "div2")]/p/text()').extract()
            item['datepost'] = site.xpath('div[@class="jobdetailWrapper"]/div[contains(@class, "jobListJInfo") and contains(@class, "div4")]/span/text()').extract()
            items.append(item)
        return items
            
