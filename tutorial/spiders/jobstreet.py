from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from tutorial.items import JobItem

class JobSpider(CrawlSpider):
    name = "jobstreet"
    allowed_domains = ["job-search.jobstreet.com.sg"]
    start_urls = [
        "http://job-search.jobstreet.com.sg/singapore/job-opening.php"
    ]

    rules = (
        # Extract links matching 'item.php' and parse them with the spider's method parse_item
        Rule(SgmlLinkExtractor(allow=('http://job-search.jobstreet.com.sg/singapore/job-opening.php', )), callback='parse_item',  follow= True),
    )

    def parse_item(self, response):
        sel = Selector(response)
        sites = sel.xpath('//*[@class="rRow"]')
        items = []
        for site in sites:
            item = JobItem()
            item['jobtitle'] = site.xpath('div[@class="rRowJob"]/h4/a/text()').extract()
            item['company'] =site.xpath('div[@class="rRowJob"]/h5/a/text()').extract()
            item['datepost'] = site.xpath('div[@class="rRowDate"]/text()').extract()
            item['industry'] = site.xpath('div[contains(@class, "rRowSpez") and contains(@class, "px10")]/a[1]/text()').extract()
            items.append(item)
            
            return items
