# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'tutorial'

SPIDER_MODULES = ['tutorial.spiders']
NEWSPIDER_MODULE = 'tutorial.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'tutorial (+http://www.yourdomain.com)'


USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36"
DOWNLOAD_DELAY = 2
DEFAULT_REQUEST_HEADERS = {
    'Cookie' : 'D_SID=116.88.212.68; gatcmr=SGLOGGED; intJSlog=m%3Atu%3A%22uEt5tu6DtDEE%22%3B; TNETSISREP=h%3At%3A%7Bm%3AuB%3A%22SFUa_bU9P_N9PUKUa9La_7MMRK9%22%3Bh%3A0%3A%7Bm%3AB%3A%22oml3_Yk%22%3Bk%3A6EDAB6t5BA%3Bm%3AD%3A%22ml3pl3_Yk%22%3BY%3AtEE%3Bm%3A0%3A%22nzvly%22%3Bm%3A5A%3A%22hBAt0BVC-A6C6-6kDC-hBVu-E06C0Vtihjti%22%3Bm%3AD%3A%22ml3Ylm_Yk%22%3Bm%3A5A%3A%225klD5uCh-6VBl-6CuE-CB60-tkkuitjDA05A%22%3Bm%3AC%3A%223lxlxil3%22%3Bi%3AE%3B%7D%7D; REFERP=h%3A0%3A%7Bm%3AtC%3A%22mYnl_whyWohWl_jzkl%22%3BY%3A5%3Bm%3A6%3A%22mYnl%22%3Bm%3Au%3A%22mW%22%3Bm%3AC%3A%22whyWohWl%22%3Bm%3A0%3A%22ly_bU%22%3Bm%3Atu%3A%22Wlyl3Yj_mYnl%22%3Bm%3Au%3A%22mW%22%3Bm%3AtC%3A%22hwwzqlk_qz3v_4lh3m%22%3BY%3AtA%3B%7D; LDSESSIONID=0dcpos0qcd31mbprlqq7g3r721; YROTSIH=h%3At%3A%7Bm%3AuE%3A%22QMGU9FP7J_U9UUKML_K8%22%3Bm%3AuA%3A%22Ekj1zmE2jk5txi13w22BW53But%22%3B%7D; NSC_qfstjtu_kpc-tfbsdi=ffffffff091c0a1745525d5f4f58455e445a4a423660; SEARCH_REMEMBER=h%3A5%3A%7BY%3AE%3Bh%3A5%3A%7Bm%3AB%3A%22vl4qz3k%22%3Bm%3AA%3A%2214nXzy%22%3Bm%3A5%3A%22o3w%22%3Bm%3At0B%3A%22Xnn1%3A%2F%2FZzi-mlh3jX.Zzimn3lln.jzx.mW%2FmYyWh1z3l%2FZzi-z1lyYyW.1X1%3Fvl4%3D14nXzy%26h3lh%3Dt%26z1nYzy%3Dt%26Zzi-mzo3jl%3Dt%25u7A6%26jwhmmYVYlk%3Dt%26Zzi-1zmnlk%3DE%26m3j%3DB%261W%3Dt%26mz3n%3Du%26z3kl3%3DE%22%3Bm%3A0%3A%22nYnwl%22%3Bm%3AA%3A%2214nXzy%22%3B%7DY%3At%3Bh%3A5%3A%7Bm%3AB%3A%22vl4qz3k%22%3Bm%3AD%3A%22klplwz1l3%22%3Bm%3A5%3A%22o3w%22%3Bm%3AtAE%3A%22Xnn1%3A%2F%2FZzi-mlh3jX.Zzimn3lln.jzx.mW%2FmYyWh1z3l%2FZzi-z1lyYyW.1X1%3Fvl4%3Dklplwz1l3%26h3lh%3Dt%26z1nYzy%3Dt%26Zzi-mzo3jl%3Dt%25u7A6%26jwhmmYVYlk%3Dt%26Zzi-1zmnlk%3DE%26m3j%3DB%261W%3Dt%26mz3n%3Du%26z3kl3%3DE%22%3Bm%3A0%3A%22nYnwl%22%3Bm%3AD%3A%22klplwz1l3%22%3B%7DY%3Au%3Bh%3A5%3A%7Bm%3AB%3A%22vl4qz3k%22%3Bm%3A5%3A%221X1%22%3Bm%3A5%3A%22o3w%22%3Bm%3At06%3A%22Xnn1%3A%2F%2FZzi-mlh3jX.Zzimn3lln.jzx.mW%2FmYyWh1z3l%2FZzi-z1lyYyW.1X1%3Fvl4%3D1X1%26h3lh%3Dt%26z1nYzy%3Dt%26Zzi-mzo3jl%3Dt%25u7A6%26jwhmmYVYlk%3Dt%26Zzi-1zmnlk%3DE%26m3j%3DB%261W%3Dt%26mz3n%3Du%26z3kl3%3DE%22%3Bm%3A0%3A%22nYnwl%22%3Bm%3A5%3A%221X1%22%3B%7D%7D; SEARCH_REMEMBER_LAST=%3Ca+href%3D%22http%3A%2F%2Fjob-search.jobstreet.com.sg%2Fsingapore%2Fjob-opening.php%3Fkey%3Dphp%26area%3D1%26option%3D1%26job-source%3D1%252C64%26classified%3D1%26job-posted%3D0%26src%3D38%26pg%3D1%26sort%3D2%26order%3D0%22+title%3D%22php%22%3Ephp%3C%2Fa%3E; HCRSEABOJ=h%3At%3A%7Bm%3AA%3A%22mhplk2%22%3Bm%3A6t%3A%22+%2B%28mYnlm%3Au%29+%2B%28Zzi_mzo3jl_jzkl%3A%28t+A6+tuC%29%29%22%3B%7D; SEARCH_VIEW_TYPE=detail; __utma=84921862.396751064.1386087312.1393409054.1393483908.23; __utmb=84921862.1.10.1393483908; __utmc=84921862; __utmz=84921862.1391115228.15.5.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); D_PID=54B9EC2B-361A-3BFD-9BAD-578F133833C3; D_UID=34DD3DD8-5D0A-3B87-A825-9BE2C5FD897A; D_IID=A61BCEB1-82F2-3118-8A69-E060CEBAC60C; WT_FPC=id=22703ca7-dd9f-4194-a32d-4e26ea84bb4f:lv=1393483933397:ss=1393483908180',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en',
}
